const { request, response } = require('express')
const express = require('express')
const fs = require('fs')
const { resolve } = require('path')

const app = express()
const port = 3000

const config = JSON.parse(fs.readFileSync('config.json', 'utf-8'))
const token = config['token']
const jump_port = config['port']
let jump_ip = '127.0.0.1'

var bodyParser = require('body-parser')
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

app.get('/', (req, res) => {
    console.log(req.ip)
    res.set('Content-Type','text/html');
    const content = `
	<html>
		<head>
			<script language="javascript" type="text/javascript">window.location.href="http://${jump_ip}:${jump_port}";</script>
		</head>
	</html>
	`
    res.send(content)
})

app.post('/set', (req, resp) => {
    let request_token = req.body ? req.body.token : null
    if (request_token == null || request_token != token) {
        console.log('auth fail')
        resp.send('done')
        return
    }

    let request_ip = (req.body? req.body.ip : null) || req.headers['x-forwarded-for'] || req.connection.remoteAddress
    if (request_ip.substr(0, 7) == "::ffff:") {
        request_ip = request_ip.substr(7)
    }

    console.log(`update request ip ${request_ip}`)
    jump_ip = request_ip
    resp.send('done')
    return
})

app.listen(port, () => {
    console.log('hello')
})