import requests
import json
import traceback
import os

with open('user_config.json', encoding='utf-8') as f:
    config = json.loads(f.read())

def get_ip():
    os.system('curl -4 ip.sb > ip.txt')
    with open('ip.txt') as f:
        return f.read().removesuffix('\n')

os.system(f'''curl {config['host']}/set -X POST -d 'token={config['token']}' -d 'ip={get_ip()}' -v -k''')